<div class="modal fade bd-example-modal-md" data-backdrop="static" data-keyboard="false"  id="modal-crear-turno" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content card-info card">
            <div class="modal-header card-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear turno</h5>
                <img src="iconos-svg/time-management.svg" width="40px" class="mr-2">
            </div>
            <div class="modal-body card-body">
            
                <div class="form-group ">
                    <label for="inputPassword3">turno</label>
                    <div class="col-sm-12">
                        <input type="time" class="form-control" id="time_turno"  require>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Activo</label>
                      </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="guardar_turno"><i class="far fa-save"></i>Guardar</button>
                <button type="button" class="btn btn-danger"  id="cerrar_turno_crear" data-dismiss="modal"><i class="far fa-times-circle"></i>Cerrar</button>
            </div>
        </div>
    </div>
</div>