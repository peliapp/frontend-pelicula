<div class="modal fade bd-example-modal-md" data-backdrop="static" data-keyboard="false"  id="modal-editar-turno" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content card-info card">
            <div class="modal-header card-header">
                <h5 class="modal-title" id="exampleModalLabel">editar turno</h5>
                <img src="iconos-svg/time.svg" width="40px" class="mr-2">
            </div>
            <div class="modal-body card-body">
            
                <div class="form-group ">
                    <label for="inputPassword3">turno</label>
                    <div class="col-sm-12">
                        <input type="text" id="id_turno"  class="form-control" disabled="true" class="col-11" style="display:none">
                        <input type="time" class="form-control" id="time_turno_edit"  require>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1_edit" value="false">
                        <label class="form-check-label" for="exampleCheck1_">Activo</label>
                      </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="guardar_edit_turno"><i class="far fa-save"></i>Guardar</button>
                <button type="button" class="btn btn-danger" id="cerrar_turno_modal_edit"data-dismiss="modal"><i class="far fa-times-circle"></i>Cerrar</button>
            </div>
        </div>
    </div>
</div>