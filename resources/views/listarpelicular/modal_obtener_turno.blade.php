<div class="modal fade bd-example-modal-md" data-backdrop="static" data-keyboard="false"  id="modal-obtener-turno" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content card-info card">
            <div class="modal-header card-header">
                <h5 class="modal-title" id="exampleModalLabel">Obtener Turno</h5>
               
            </div>
            <div class="modal-body card-body">
                <div id='jqxWidget'>
                    <div id="grid_obtener_turno"></div>
                    <div style="margin-top: 30px;">
                        <div id="cellbegineditevent"></div>
                        <div style="margin-top: 10px;" id="cellendeditevent"></div>
                    </div>
                </div>
              
                
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrarObtener"><i class="far fa-times-circle"></i>Cerrar</button>
            </div>
        </div>
    </div>
</div>