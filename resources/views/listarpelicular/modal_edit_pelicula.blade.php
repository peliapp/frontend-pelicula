<div class="modal fade bd-example-modal-md" data-backdrop="static" data-keyboard="false"  id="modal-edit-pelicula" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content card-info card">
            <div class="modal-header card-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Pelicula</h5>
                <img src="iconos-svg/clapperboard.svg" width="30px" class="mr-2">
            </div>
            <div class="modal-body card-body">
                
            <div id="data">
                <form>
                    <div class="form-group ">
                        <input type="text" class="form-control" id="idPelicula" name="idPelicula" disabled="true" class="col-11" style="display:none" require >
                        <label for="inputPassword3">Nombre</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="nomb_peli_edit" name="nombre_edit" placeholder="INGRESE EL NOMBRE DE LA PELICULA" require >
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="inputPassword3">Fecha Publicacion</label>
                        <div class="col-sm-12">
                            <input type="date" class="form-control" id="fech_publ_edit"  name="publicacion_edit"  require>
                        </div>
                    </div>
                    <img id="imgSalida_edit" width="50%" height="50%" src="" />
                    <input type="file" name="file_edit" id="file_edit" size="20" />
                    <br /><br />
                   <!-- <input type="button" id="upload" value="upload" />-->
                </form>
            </div>

            


         
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="upload_edita"><i class="far fa-save"></i>Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>Cerrar</button>
            </div>
        </div>
    </div>
</div>