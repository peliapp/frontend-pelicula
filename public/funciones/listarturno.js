var data = [];
var dataAdapter = "";
function listar_turno() {
    $.ajax({
        type: "GET",
        url: "http://127.0.0.1:2023/api/listarturno",
        dataType: "json",
        success: function (responses) {
            var source = {
                localdata: responses.data,
                datatype: "array",
                datafields: [
                    { name: "idturno", type: "number" },
                    { name: "varfechturno", type: "String" },
                    { name: "idestado", type: "String" },
                    { name: "varNombrEstado", type: "String" },
                  
                ],
                updaterow: function (rowid, rowdata, commit) {
                    commit(true);
                },
            };
            console.log(source);
            dataAdapter = new $.jqx.dataAdapter(source);
            var editrow = -1;


            var opcion_archivo = function (row, column, value, rowKey, rowData) {
                var editrow = row;
                var dataRecord = $("#grid").jqxGrid('getrowdata', editrow);
                var archivo="";
                    //console.log(dataRecord); 
                    if (dataRecord.varNombrEstado==="ACTIVO"){
                        archivo = '<center><button class="btn btn-danger btn-sm" onClick=EditarTurno("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-edit"></i></button>'+
                                    '<button class="btn btn-danger btn-sm" onClick=EditarEstado("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-unlock"></i></button>'+
                                    '<button class="btn btn-danger btn-sm" onClick=Eliminar("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-trash-alt"></i></button></center>';
                        }else{
                            archivo = '<center><button class="btn btn-danger btn-sm" onClick=EditarTurno("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-edit"></i></button>'+
                                    '<button class="btn btn-danger btn-sm" onClick=EditarEstado("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-lock"></i></button>'+
                                    '<button class="btn btn-danger btn-sm" onClick=Eliminar("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-trash-alt"></i></button></center>';
                        }
                   

                return archivo;
            };



            $("#grid").jqxGrid({
                width: "70%",
                height: "300",
                source: dataAdapter,
                columnsresize: true,
                altrows: true,
                showfilterrow: true,
                filterable: true,
                selectionmode: "multiplecellsextended",
                sortable: true,
                theme: "darkblue",
                showstatusbar: true,
                statusbarheight: 25,
                showaggregates: true,
                showgroupaggregates: true,
                columns: [
                    { text: "#", datafield: "idturno", width: '10%' ,cellsalign: 'center'},
                    { text: "turno", datafield: "varfechturno", width: '30%' ,cellsalign: 'center' },
                    { text: "idestado", datafield: "idestado", width: '30%',hidden:true },
                    { text: "Estado", datafield: "varNombrEstado", width:'30%',cellsalign: 'center' },
                    {text: 'Opcion',width: '30%', datafield: 'Descargar', cellsrenderer: opcion_archivo, cellsalign: 'center'},
                ],
            });
        },
    });
}


$('#agregarturno').click(function () {
    console.log("asdasd");
    
    $('#modal-crear-turno').modal('show');
});

$("#guardar_turno").click(function(){
    let hora =  $('#time_turno').val();
    let estado = document.getElementById("exampleCheck1").checked;
    guardarTurno(hora,estado)
})

function guardarTurno(hora,estado){
    $.ajax({
        type: 'POST',
        url: 'http://127.0.0.1:2023/api/guardarturno',
        dataType: 'json',
        data: {
            varfechturno: hora,
            estado:estado,
           
        },
        
        success: function ({message}) {
            LimpiarCampos();
            mostrarsucces(message);
            $('#modal-crear-turno').modal('hide');
            listar_turno();
               
        }
    });
}


function mostrarsucces (message){
    Swal.fire({
        title: '', 
        html: message,  
        confirmButtonText: "Aceptar", 
      });
}

function LimpiarCampos(){
    $('#time_turno').val('');
    document.getElementById("exampleCheck1").checked=false;
}


function EditarTurno(idgalv){

    let dataRecord = $("#grid").jqxGrid('getrowdata', idgalv);
    
    $('#id_turno').val(dataRecord.idturno);
    $('#time_turno_edit').val(dataRecord.varfechturno);

    if(dataRecord.idestado=="1"){
        
        $("#exampleCheck1_edit").prop('checked', true);
       
    }else{
        $("#exampleCheck1_edit").prop('checked', false);
    }  
   
    $('#modal-editar-turno').modal('show');
   
}


// function cerrar 
function cerrar_turno_modal_edit(){
    $("#modal-editar-turno").modal('hide');
    LimpiarCampos();
}

$("#cerrar_turno_crear").click(function(){
    $("#modal-crear-turno").modal('hide');
    $("#time_turno").val('');
    $("#exampleCheck1").prop('checked', false);
    
    
})

$("#guardar_edit_turno").click(function(){
    
    let idturno  = $("#id_turno").val();
    let hora_editar =  $('#time_turno_edit').val();
    let estado_editar = document.getElementById("exampleCheck1_edit").checked;
    Editar_turno(idturno,hora_editar,estado_editar)
})

function Editar_turno(idturno,hora_editar,estado_editar){
    $.ajax({
        type: 'POST',
        url: 'http://127.0.0.1:2023/api/editarturno',
        dataType: 'json',
        data: {
            idturno:idturno,
            varfechturno: hora_editar,
            estado:estado_editar,
           
        },
        
        success: function ({message}) {
            LimpiarCampos();
            mostrarsucces(message);
            $('#modal-editar-turno').modal('hide');
            listar_turno();
               
        }
    });
}

function EditarEstado(idgalv){

    let dataRecord = $("#grid").jqxGrid('getrowdata', idgalv);
    
    editaEstado(dataRecord.idturno)
}

function editaEstado(idturn){
   
    $.ajax({
        type: 'POST',
        url: 'http://127.0.0.1:2023/api/editaestado',
        dataType: 'json',
        data: {
            idturno:idturn,
        },
        
        success: function ({message}) {
            
            mostrarsucces(message);
            listar_turno();
               
        }
    });
}

function mostrarsucces (message){
    Swal.fire({
        title: '', 
        html: message,  
        confirmButtonText: "Aceptar", 
      });
}


function Eliminar(idgalv){
    let dataRecord = $("#grid").jqxGrid('getrowdata', idgalv);
    EliminarTurno(dataRecord.idturno);
}


function EliminarTurno(idturn){
   
    $.ajax({
        type: 'POST',
        url: 'http://127.0.0.1:2023/api/eliminarturno',
        dataType: 'json',
        data: {
            idturno:idturn,
        },
        
        success: function ({message}) {
            
            mostrarsucces(message);
            listar_turno();
               
        }
    });
}