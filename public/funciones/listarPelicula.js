var data = [];
var dataAdapter = "";
var result ="";
var formData = new FormData();

function listar_pelicula() {
    $.ajax({
        type: "GET",
        url: "http://127.0.0.1:2023/api/Pelicula",
        dataType: "json",
        success: function (responses) {
            var source = {
                localdata: responses.data,
                datatype: "array",
                datafields: [
                    { name: "idPelicula", type: "String" },
                    { name: "nombre", type: "String" },
                    { name: "publicacion", type: "String" },
                    { name: "urlImagen", type: "String" },
                    { name: "created_at", type: "String" },
                    { name: "update_at", type: "String" },
                    { name: "idestado", type: "String" },
                    { name: "varNombrEstado", type: "String" },
                ],
                updaterow: function (rowid, rowdata, commit) {
                    commit(true);
                },
            };
            
            dataAdapter = new $.jqx.dataAdapter(source);
            var editrow = -1;
            var opcion_archivo = function (row, column, value, rowKey, rowData) {
                var editrow = row;
                var dataRecord = $("#grid").jqxGrid('getrowdata', editrow);
                var archivo="";
                    //console.log(dataRecord); 
                    if (dataRecord.varNombrEstado==="ACTIVO"){
                        archivo =   '<center><button class="btn btn-danger btn-sm" onClick=editarPelicula("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-edit"></i></button>'+
                                    '<button class="btn btn-danger btn-sm" onClick=ObtenerTurno("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-bars"></i></button>'+
                                    '<button class="btn btn-danger btn-sm" onClick=Estado("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-unlock"></i></button>'+
                                    '<button class="btn btn-danger btn-sm" onClick=Eliminar("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-trash-alt"></i></button></center>';
                        }else{
                            archivo = '<center><button class="btn btn-danger btn-sm" onClick=editarPelicula("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-edit"></i></button>'+
                                    '<button class="btn btn-danger btn-sm" onClick=ObtenerTurno("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-bars"></i></button>'+   
                                    '<button class="btn btn-danger btn-sm" onClick=Estado("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-lock"></i></button>'+
                                    '<button class="btn btn-danger btn-sm" onClick=Eliminar("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-trash-alt"></i></button></center>';
                        }
                   

                return archivo;
            };


            $("#grid").jqxGrid({
                width: "90%",
                height: "300",
                source: dataAdapter,
                columnsresize: true,
                altrows: true,
                showfilterrow: true,
                filterable: true,
                selectionmode: "multiplecellsextended",
                sortable: true,
                theme: "darkblue",
                showstatusbar: true,
                statusbarheight: 25,
                showaggregates: true,
                showgroupaggregates: true,
                columns: [
                    { text: "ID", datafield: "idPelicula", width: 180 },
                    { text: "Pelicula", datafield: "nombre", width: 180 },
                    { text: "Estreno", datafield: "publicacion", width: 180 },
                    { text: "idestado", datafield: "idestado", width: 180,hidden:true },
                    { text: "Estado", datafield: "varNombrEstado", width: 180,},

                    { text: "Imagen", datafield: "urlImagen", width: 130,hidden:true },
                    { text: "created_at", datafield: "created_at", width: 150 },
                    { text: "update_at", datafield: "update_at", width: 150 },
                    {text: 'Opcion',width: '30%', datafield: 'Descargar', cellsrenderer: opcion_archivo, cellsalign: 'center'},
                ],
            });
        },
    });
}


$('#agregarPelicula').click(function () {
    
    $('#modal-crear-pelicula').modal('show');
});


// $("#guardar_pelicula").click(function(){
//     //formData.append('nombre', $('#nomb_peli').val().toUpperCase().trim());
//    // formData.append('publicacion', $('#fech_publ').val());
    
//     for (var pair of formData.entries()) {
//         console.log(pair[0]+ ', ' + pair[1]); 
//     }


//     // console.log(formData);
//     // let nombre = $('#nomb_peli').val().toUpperCase().trim();
//     // let fechapub = $('#fech_publ').val();
    
//      guardar_peli(formData)
  
// });


$(window).load(function(){

    $(function() {
     $('#file').change(function(e) {
      
         addImage(e); 
        });
   
        function addImage(e){
         var file = e.target.files[0],
         
         imageType = /image.*/;
    
            
         if (!file.type.match(imageType))
          return;
         var reader = new FileReader(); 
         reader.onload = fileOnload;
         reader.readAsDataURL(file);
        }
     
        function fileOnload(e) {
        

         $('#imgSalida').attr("src",e.target.result);
        }
       });
 });



//  function guardar_peli(formData) {
//     console.log(formData)
//     $.ajax({
//         type: 'POST',
//         url:'http://127.0.0.1:2023/api/GuardarPelicula',
//         dataType: 'json',
//         processData: false,
//         contentType: false,
//         data:formData,
//         //contentType: 'multipart/form-data', 

//         success: function (responses) {
//             console.log(responses);
//         }
//     });
// }
   
$('#upload').click(function(){

    console.log('upload button clicked!')
    var fd = new FormData();    
    fd.append('nombre', $('#nomb_peli').val().toUpperCase().trim());
    fd.append( 'publicacion', $('#fech_publ').val());
    fd.append( 'file', $('#file')[0].files[0]);

    $.ajax({
      url: 'http://127.0.0.1:2023/api/GuardarPelicula',
      data: fd,
      processData: false,
      contentType: false,
      type: 'POST',
      success: function({message}){
           
            mostrarsucces(message);
            limpiarCamposcrear();
            listar_pelicula();
           
            $("#modal-crear-pelicula").modal('hide');
      }
    });
});

function limpiarCamposcrear(){
    $('#nomb_peli').val('');
    $('#fech_publ').val('');
    $('#file').val('');
    document.getElementById("file").setAttribute("src", "");
    $('#file')[0].files[0]="";
}


function mostrarsucces (message){
    Swal.fire({
        title: '', 
        html: message,  
        confirmButtonText: "Aceptar", 
      });
}

//cambiarEstadoPelicula
function Estado(idgalv){
    let dataRecord = $("#grid").jqxGrid('getrowdata', idgalv);

    console.log(dataRecord)
    cambiarEstado(dataRecord.idPelicula)
}

function cambiarEstado(idpelicu){
    $.ajax({
        type: 'POST',
        url: 'http://127.0.0.1:2023/api/cambiarEstadoPelicula',
        dataType: 'json',
        data: {
            idPelicula:idpelicu,
        },
        
        success: function ({message}) {
            
            mostrarsucces(message);
            listar_pelicula();
               
        }
    });

}

function Eliminar (idgalv){
    let dataRecord = $("#grid").jqxGrid('getrowdata', idgalv);
    PeliculaEliminado(dataRecord.idPelicula)
}


function PeliculaEliminado(idpeli){
    $.ajax({
        type: 'POST',
        url: 'http://127.0.0.1:2023/api/EliminarPelicula',
        dataType: 'json',
        data: {
            idPelicula:idpeli,
        },
        
        success: function ({message}) {
            
            mostrarsucces(message);
            listar_pelicula();
               
        }
    });
}

$("#cerrarObtener").click(function(){
    $('#modal-obtener-turno').modal('hide');
    $("#grid_obtener_turno").jqxGrid('clear');
})

function ObtenerTurno (idgalv){
    let dataRecord = $("#grid").jqxGrid('getrowdata', idgalv);
    mostrarModalTurnos(dataRecord.idPelicula);

  

}

function mostrarModalTurnos(peliculaid){
    $.ajax({
        type: 'POST',
        url: 'http://127.0.0.1:2023/api/listaTurno',
        dataType: 'json',
        data: {
            idPelicula:peliculaid,
        },
        
        success: function ({message,data}) {
            
            var source = {
                localdata: data,
                datatype: "array",
                datafields: [
                    { name: "idPelicula", type: "Number" },
                    { name: "idturno", type: "Number" },
                    { name: "varfechturno", type: "String" },
                    { name: "valor", type: "Number" },
                    
                    
                    
                ],
                updaterow: function (rowid, rowdata, commit) {
                    commit(true);
                },
            };
            
            $("#grid_obtener_turno").jqxGrid('clear');
            dataAdapter = new $.jqx.dataAdapter(source);
            var editrow = -1;
            var opcion_archivo = function (row, column, value, rowKey, rowData) {
                var editrow = row;
                var dataRecord = $("#grid_obtener_turno").jqxGrid('getrowdata', editrow);
                var archivo="";
                    //console.log(dataRecord); 
                    if (dataRecord.valor=="1"){
                            archivo =   '<center><button class="btn btn-danger btn-sm" onClick=ActivarTurno("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: white;" ><i class="fas fa-check"></i></button>';          ;
                            }else{
                            archivo = '<center><button class="btn btn-danger btn-sm" onClick=ActivarTurno("' + editrow + '"); style="margin-left: 3px;color: #001255;background-color: green;" ><i class="fas fa-check"></i></button>';
                        }
                   

                return archivo;
            };


            $("#grid_obtener_turno").jqxGrid({
                width: "70%",
                height: "300",
                source: dataAdapter,
                columnsresize: true,
                altrows: true,
                showfilterrow: true,
                filterable: true,
                selectionmode: "multiplecellsextended",
                sortable: true,
                theme: "darkblue",
                showstatusbar: true,
                statusbarheight: 25,
                showaggregates: true,
                showgroupaggregates: true,
                columns: [
                    { text: "Idpelicula", datafield: "idPelicula", width: 180,hidden:true },
                    { text: "ID", datafield: "idturno", width: 180,hidden:true },
                    { text: "varfechturno", datafield: "varfechturno", width:"50%" ,cellsalign:'center' },
                    {text: 'Opcion',width: '50%', datafield: 'Descargar', cellsrenderer: opcion_archivo, cellsalign: 'center'},
                ],
            });
               
        }
    });
    


    $('#modal-obtener-turno').modal('show');
}


function ActivarTurno(idgalv){
    let dataRecord = $("#grid_obtener_turno").jqxGrid('getrowdata', idgalv);
    console.log(dataRecord)

    $.ajax({
        type: 'POST',
        url: 'http://127.0.0.1:2023/api/activandacheckdp',
        dataType: 'json',
        data: {
            idPelicula:dataRecord.idPelicula,
            idturno:dataRecord.idturno,
            valor:dataRecord.valor,
        },
        
        success: function ({message}) {
            
            mostrarsucces(message);
            mostrarModalTurnos(dataRecord.idPelicula);
               
        }
    });
}



//PARA EDITAR 
function editarPelicula(idgalv){
    let dataRecord = $("#grid").jqxGrid('getrowdata', idgalv);

    console.log(dataRecord)
    $("#idPelicula").val(dataRecord.idPelicula);
    $("#nomb_peli_edit").val(dataRecord.nombre);
    $("#fech_publ_edit").val(dataRecord.publicacion);
    
    $("#modal-edit-pelicula").modal('show');
}

$(window).load(function(){

    $(function() {
     $('#file_edit').change(function(e) {
      
         addImage(e); 
        });
   
        function addImage(e){
         var file = e.target.files[0],
         
         imageType = /image.*/;
    
            
         if (!file.type.match(imageType))
          return;
         var reader = new FileReader(); 
         reader.onload = fileOnload;
         reader.readAsDataURL(file);
        }
     
        function fileOnload(e) {
        

         $('#imgSalida_edit').attr("src",e.target.result);
        }
       });
 });




$('#upload_edita').click(function(){

   
    var fd = new FormData();
    fd.append('idPelicula', $('#idPelicula').val());
    fd.append('nombre_edit', $('#nomb_peli_edit').val().toUpperCase().trim());
    fd.append( 'fech_publ_edit', $('#fech_publ_edit').val());
    fd.append( 'file_edit', $('#file_edit')[0].files[0]);

    $.ajax({
      url: 'http://127.0.0.1:2023/api/EditaPelicula',
      data: fd,
      processData: false,
      contentType: false,
      type: 'POST',
      success: function({message}){
           
            mostrarsucces(message);
            limpiarCamposcrearEditar();
            listar_pelicula();
            $("#modal-edit-pelicula").modal('hide');
      }
    });
});

function limpiarCamposcrearEditar(){
    $("#idPelicula").val('');
    $("#nomb_peli_edit").val('');
    $("#fech_publ_edit").val('');
    
}